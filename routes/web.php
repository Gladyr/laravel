<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('/users') -> name('users.')-> group (function(){
    Route::get('/{id}', [UserController::class, 'getUser'])->name('getOne');
    Route::get('/',[UserController::class,'index'])->name('list');
    Route::post('/', [UserController::class, 'create'])->name('new');
    Route::put('/', [UserController::class, 'edit'])->name('change');
    Route::get('/delete/{id}',[UserController::class, 'create'])->name('drop');

});

